"use strict";

var Todo = require("../models/todo");

var TodoCollection = Backbone.Collection.extend({
    model: Todo
});

module.exports = TodoCollection;