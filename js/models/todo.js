"use strict";

var Todo = Backbone.Model.extend({
    defaults: {
        title: "",
        completed: false
    }
});

module.exports = Todo;