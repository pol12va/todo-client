"use strict";

var TodoCollection = require("../collections/todos");

var TodoList = Backbone.Model.extend({
    defaults: {
        filterType: "all",
        todos: []
    },

    serializeData: function () {
        return this.toJSON();
    },

    getTodos: function () {
        if (!(this.get('todos') instanceof TodoCollection)) {
            this.set('todos', new TodoCollection(this.get('todos') || null));
        }

        return this.get('todos');
    }
});

module.exports = TodoList;