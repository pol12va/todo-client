"use strict";

(function () {
    var $ = require('jquery'),
        TodoList = require("./models/todo-list"),
        AppView = require("./views/app"),
        items,
        appData,
        todoList,
        app;

    items = localStorage.getItem('todo');

    appData = items ? JSON.parse(localStorage.getItem('todo')) : {};

    todoList = new TodoList(appData, { parse: true });

    app = new AppView({
        el: $('.todo-container')[0],
        model: todoList,
        title: 'Todos'
    });
})();
