"use strict";

var TodoView;

TodoView = Backbone.View.extend({
    tagName: 'li',

    className: 'list-item',

    template: require('../templates/todo.ejs'),

    initialize: function (options) {
        this.todoCollection = options.todoCollection;

        this.listenTo(this.model, 'destroy', this.remove);
        this.listenTo(this.model, 'change:completed', this.todoCompleted);
        this.listenTo(this.todoCollection.getTodos(), 'remove', this.todoRemoved);
        this.listenTo(this.todoCollection.getTodos(), 'filter:applied', this.filterApplied);

        this.render();
    },

    events: {
        'click input[type="checkbox"]' : 'expunge',
        'mouseover' : 'showRemoveButton',
        'mouseout' : 'hideRemoveButton',
        'click .remove' : 'removeView'
    },

    render: function () {
        this.$el.html(this.template(this.serializeData()));

        this.initUi();

        return this;
    },

    initUi: function () {
        this.$remove = this.$('.remove');
        this.$text = this.$('.todo-text');
        this.$checkbox = this.$('input[type="checkbox"]');
    },

    expunge: function (e) {
        if (this.$checkbox.prop('checked')) {
            this.model.set('completed', true);
        } else {
            this.model.set('completed', false);
        }

        this.filterApplied(this.todoCollection.get('filterType'));
    },

    showRemoveButton: function () {
        this.$remove.css('display', 'inline-block');
    },

    hideRemoveButton: function () {
        this.$remove.css('display', 'none');
    },

    todoRemoved: function (todo) {
        if (todo === this.model) {
            this.remove();
        }
    },

    removeView: function () {
        this.model.destroy();
    },

    filterApplied: function (filterType) {
        if (filterType === "active") {
            this.model.get('completed') ? this.$el.hide() : this.$el.show();
        } else if (filterType === "completed") {
            this.model.get('completed') ? this.$el.show() : this.$el.hide();
        } else {
            this.$el.show();
        }
    },

    todoCompleted: function () {
        this.$text.toggleClass('expunge');
    },

    serializeData: function(){
        return this.model.toJSON();
    }
});

module.exports = TodoView;