"use strict";

var $ = require('jquery'),
    ListPanelView;

ListPanelView = Backbone.View.extend({
    tagName: 'footer',

    template: require('../templates/panel.ejs'),

    events: {
        'click li': 'applyFilter'
    },

    initialize: function () {
        this.listenTo(this.model.getTodos(), 'add', this.collectionChanged);
        this.listenTo(this.model.getTodos(), 'remove', this.collectionChanged);
        this.listenTo(this.model.getTodos(), 'change:completed', this.todoCompleted);

        this.render();
    },

    render: function () {
        var $filter,
            filterType;

        this.$el.html(this.template({
            size: this.model.getTodos().where({ completed: false }).length
        }));

        if (!this.model.getTodos().size()) {
            this.$el.hide();
        }

        filterType = this.model.get('filterType');

        if (filterType) {
            $filter = this.$('[type="' + filterType.toLowerCase() + '"]');

            if (!$filter || !$filter.length) {
                $filter = this.$('[type="all"]');
            }

            $filter.addClass('selected');

            this.model.trigger('change:filterType', this.model.get('filterType'));
        } else {
            $filter = this.$('[type="all"]');
            this.$('[type="all"]').addClass('selected');
        }

        this.initUi();

        return this;
    },

    initUi: function () {
        this.$counter = this.$('.counter');
        this.$selected = this.$('.selected');
    },

    collectionChanged: function () {
        var size = this.model.getTodos().where({'completed' : false}).length;

        if (!size) {
            this.$el.hide();
        } else {
            this.$counter.html(this.getCounterText(size));
            this.$el.show();
        }
    },

    applyFilter: function (e) {
        var target = $(e.target);

        this.$selected.removeClass('selected');

        target.addClass('selected');

        this.$selected = target;

        this.model.set('filterType', target.attr('type'));
    },

    todoCompleted: function () {
        var size = this.model.getTodos().where({'completed' : false}).length;

        this.$counter.html(this.getCounterText(size));
    },

    getCounterText: function (size) {
        return size + (size > 1 ? " items" : " item") + " left";
    }
});

module.exports = ListPanelView;