"use strict";

var TodoView = require("./todo"),
    ListPanelView = require("./list-panel"),
    TodoListView;

TodoListView = Backbone.View.extend({
    className: 'todo-list',

    template: require('../templates/todos.ejs'),

    initialize: function () {
        this.listenTo(this.model.getTodos(), 'add', this.addChild);
        this.listenTo(this.model, 'change:filterType', this.filterApplied);
    },

    addChild: function (child) {
        var todoView = new TodoView({ model: child, todoCollection: this.model });

        this.$listRoot.append(todoView.el);

        if (this.model.get('filterType') !== 'all') {
            if (this.model.get('filterType') === 'completed' && !child.get('completed')) {
                todoView.$el.hide();
            } else if (this.model.get('filterType') === 'active' && child.get('completed')) {
                todoView.$el.hide();
            }
        }

    },

    render: function () {
        this.$el.html(this.template());

        this.initUi();

        this.model.getTodos().each(this.addChild, this);

        this.filterView = new ListPanelView({ model: this.model });

        this.$listRoot.after(this.filterView.el);

        return this;
    },

    initUi: function () {
        this.$listRoot = this.$('.list-root');
    },

    filterApplied: function () {
        var filterType = this.model.get('filterType');

        this.model.getTodos().trigger('filter:applied', filterType);
    }
});

module.exports = TodoListView;