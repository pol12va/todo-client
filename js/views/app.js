"use strict";

var $ = require('jquery'),
    TodoListView = require("./todo-list"),
    Todo = require("../models/todo");

var AppView = Backbone.View.extend({
    template: require("../templates/app.ejs"),

    events: {
        'keyup .new-todo-input': 'addTask',
    },

    initialize: function (options) {
        this.options = options;
        this.render();

        $(window).unload(this.storeTodos.bind(this));
    },

    initUi: function () {
        this.$input = this.$('.new-todo-input');
    },

    render: function() {
        this.$el.append(this.template(this.options));
        this.$el.append(new TodoListView({ model: this.model }).render().el);

        this.initUi();

        return this;
    },

    addTask: function (e) {
        var title,
            todo,
            todoListView;

        if (e.which === 13) {
            title = this.$input.val();
            if (title) {
                todo = new Todo({
                    title: title,
                    completed: false
                });
                this.model.getTodos().add(todo);
            }
            this.$input.val('');
        }
    },

    storeTodos: function () {
        localStorage.setItem('todo', JSON.stringify(this.model.serializeData()));
    }
});

module.exports = AppView;