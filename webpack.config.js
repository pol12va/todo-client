"use strict";

const NODE_ENV = process.env.NODE_ENV || 'development';

let webpack = require('webpack'),
    srcPath = "./js",
    distPath = './dist';

module.exports = {
    entry: srcPath + "/bootstrap.js",
    output: {
        path: distPath,
        filename: "build.js"
    },

    watch: NODE_ENV === 'development',

    watchOptions: {
        aggregateTimeout: 100
    },

    devtool: NODE_ENV === 'development' ? "eval" : null,

    module: {
        loaders: [
            {
                test: /\.ejs$/, loader: 'ejs-loader'
            }
        ]
    },

    plugins: [
        new webpack.ProvidePlugin({
            _: "lodash",
            Backbone: "backbone"
        }),
        new webpack.NoErrorsPlugin()
    ],

    resolve: {
        alias: {
            'underscore': '../lodash/index.js'
        }
    }
};

if (NODE_ENV === 'production') {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        })
    );
}
